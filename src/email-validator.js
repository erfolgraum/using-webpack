// import { email, btn, form } from "./join-us-section.js"

const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

// let regex = new RegExp("[a-z0-9]+@(gmail|outlook|ukr)+\.[a-z]{2,3}$");
let regex = new RegExp("[a-z0-9]+@(gmail\.com|outlook\.com|yandex\.ru)$");

export function validate(email) {
    
  for(let address of VALID_EMAIL_ENDINGS) {
        if(email.includes(address) && regex.test(email)){
            alert(`This email ${email} is correct!`)
            return true
        } 
  }  
  alert(`This email ${email} is not valid!`)
  return false

}