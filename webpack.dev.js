const path = require("path");
const { merge } = require('webpack-merge')  
const Common = require('./webpack.common.js')


module.exports = merge(Common, {

    mode: "development",

    devServer: {
        open: true,
        port: 8000,
        compress: true,
        hot: true, //if we change smth in dist - take place autoreload a webserver
    },

});
